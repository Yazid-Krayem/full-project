# Simple Authentication

Authentication is *hard*. We will try to get down to basics here, hang on to your belts.

Implementing your own authentication system is rarely a good idea, but we still will describe the basic idea, for the purposes of illustration:

1. Front-end: create a form with user and password
2. Front-end: send the data to an express route
3. Back-end: in Express, check if the user and password match
4. Back-end: if yes, send back a unique id we can use to identify the user
5. Back-end: if no, send back an error
6. Front-end: when an answer is received, if positive, store the id in a cookie

There are many, many, many different moving parts to keep track of, including which cryptographic algorithm to use, how to handle password resets, and so on. It is never a good idea to try to do that, unless you are a seasoned professional.

In general, you'll be using [Passport](http://www.passportjs.org/). Passport allows to implement relatively easily different strategies, including logging in through Google, Twitter, Github, Facebook, and [502 other ones](http://www.passportjs.org/packages/). However, the documentation of Passport is so notoriously bad that unrelated users maintain an [external documentation](https://github.com/jwalton/passport-api-docs) just to make things easier for everyone.

Before you will implement proper Authentication in your Node app, read [Your Node Authentication Tutorial Is Wrong](https://hackernoon.com/your-node-js-authentication-tutorial-is-wrong-f1a3bf831a46) first, and take inspiration from [this implementation](https://gitlab.com/sharkattack/express-authentication). If possible at all, use a third party like [Firebase](https://firebase.google.com/) or [Auth0](https://auth0.com/).

It's impossible to steal your gold if you have no gold; similarly, always thrive to store only the least possible amount of information on your server. Delegate the responsiblity of logins to a 3rd party, whenever possible.  
Actually, don't have logins at all when your project allows it.

Here's the logic for a request to a page that should be behind a login:

```
1. *START*: the user chooses a url  
2. `/login` the user enters a user name and a password in a login page  
  1. *IF* the user is already logged in  
    1. redirect to `/`  
  2. *ELSE*, the server checks that this user name and password matches with a record  
    1. *IF* it matches  
      1. generate a secret token  
      2. store it somewhere so it can easily be looked up  
      3. send it to the user  
      4. go back to *START*  
    2. *ELSE*  
      1. send `401 UNAUTHORIZED`, possibly with an error message  
3. `/some-page` the user makes a request to an normal page  
  1. the server sends the authorized page  
  2. go back to *START*  
4. `/my-page`, the user makes a request to a protected page  
  1. *IF* the user has a secret token, she sends it with the request  
    1. *IF* the token exists also on the server, the user is guessed from the token  
      1. send the page, status `200 OK`, go back to *START*  
    2. *ELSE*  
      1. send `401 UNAUTHORIZED`  
      2. redirect to `/login`  
  2. *ELSE*, the request is made without the secret token  
      1. the server refuses the request, `401 UNAUTHORIZED`  
      2. redirect to `/login`  
5.  `/logout`, the user navigates to the logout page  
  1.  *IF* the user has a secret token, she sends it with the request  
    1. *IF* the token exists also on the server, the user is guessed from the token  
      1. remove the token on the server  
      2. send `200 OK`, go back to *START*  
    2. *ELSE*  
      1. send `200 OK`, go back to *START*  
  2. *ELSE*, the request is made without the secret token  
    1. send `200 OK`, go back to *START*  
```

Let's implement that.

We're going to need:

1. a place to store tokens. This will be a simple javascript object (later on, we can store those in the database or wherever convenient)
2. a way to authenticate users. We'll just have a static list of usernames & passes (later, we can switch to a database for this too)
3. a way to login
4. a way to ensure a user is logged in

You may not know, but routes in Express are stackable. That is, if you do

```js
const a = (req, res, next ) => next()
const b = (req, res, next ) => res.send('B')
const c = (req, res, next ) => res.send('C')
app.get('/route',a,b,c)
```
the request will go through `a`, and `b`, but not `c`

This allows us to do logged in checks relatively easily, in this way:

```js
const isLoggedIn = (req, res, next) => {
  if(something_that_checks_for_user() === true){
    next()
  }
  else{
    next(new Error('error, you are not allowed'))
  }
}

app.get('/some/protected/route', isLoggedIn, (req, res, next) => {
  //user is logged in
})
```

Armed with this information, let's do this. Create a new file, `auth.js`

```js
// back/src/auth.js

/**
 * this is our users database
 **/
const users = {
  1:{ username:'sonia@google.com', password:'1234' },
  2:{ username:'ali@facebook.com', password:'hunter2' },
  3:{ username:'ken.masters@sf.com', password:'shoryuken' }
}

/**
 * this is our logged in users.
 * In this example, `ken` is logged in (we suppose they hold the same token)
 **/
const tokens = {
  'g745jeggr':3
}

/**
 * 
 **/
const isLoggedIn = () => {

}

/**
 * 
 **/
const authenticateUser = () => {

}
```

